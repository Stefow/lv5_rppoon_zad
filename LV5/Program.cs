﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace LV5
{
    class Program
    {

        static void Main(string[] args)
        {
            double ukupno = 0;
            List<IShipable> lista = new List<IShipable>();
            Box kutija = new Box("Vrećica");
            lista.Add(kutija);
            Product product1 = new Product("Burek",12, 0.5);
            lista.Add(product1);
            Product product2 = new Product("Pizza", 10, 0.25);
            lista.Add(product2);
            Product product3 = new Product("sok", 8, 0.5);
            ShippingService price = new ShippingService(4);
            lista.Add(product3);

            foreach (IShipable ship in lista)
            {
                ukupno += ship.Weight;
                Console.WriteLine(ship.Description());
            }
            Console.WriteLine("Cijena dostave iznosi :" + price.Price(ukupno) + " kune");
        }
    }
}
