﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LV5
{
    class ShippingService
    {
        private double jedCijenaZaMasu;
        public ShippingService(double jm)
        {
            this.jedCijenaZaMasu = jm; 
        }

        public double Price(double tezina)
        {
            return jedCijenaZaMasu * tezina;
        }
    }
}
